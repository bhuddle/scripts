#!/usr/bin/python

# Script to register a Montoya DUT, skip past all the intro BS
# Created at AllionUSA to register DUT by Benjamin Huddle

import subprocess, sys, os, string
import time

class registration():
    def __init__(self):
        self.userName = "benjaminhuddle@hotmail.com"
        self.userPass = "help4me?"
        self.ApPass = "testlabs"

def checkIfDevice():
    adb = subprocess.Popen(["adb", "devices"], stdout=subprocess.PIPE)
    while adb.poll() is None:
        output = adb.stdout.readline()
        output = adb.stdout.readline()
        if "device" in output:
            if "device" in adb.stdout.readline():
                print "Multiple devices attached, exiting!"
                sys.exit(1)
            else:
                break
        else:
            print "No devices attached, exiting!"
            sys.exit(1)

    #one device at this point
    splitUp = output.split('\t')
    serialNo = splitUp[0]
    return serialNo


def adbTextCommand(keyword):
    print "adb shell input text "+keyword
    time.sleep(1)
    subprocess.Popen("adb shell input text "+keyword, shell = True)


def adbKeyCommand(keyevent):
    print "adb shell input keyevent "+keyevent
    time.sleep(5)
    subprocess.Popen("adb shell input keyevent "+keyevent, shell = True)

def skipIntroVideo():
    #key code to skip is 90
    #roughly 220 second video
    seconds = 0
    raw_input("Wait for video to start and hit ENTER")
    while seconds <= 220:
        #time.sleep(1)
        subprocess.Popen("adb shell input keyevent 90", shell = True)
        time.sleep(1)
        seconds = seconds + 10

def main():
    DUT = registration()
    checkIfDevice()

    raw_input("Pair remote and select edlab24 or edlab5 then press ENTER")

    adbTextCommand(DUT.ApPass)
    #KEYCODE_TAB  -- also the same as enter
    adbKeyCommand('61')

    print "Checking for update"
    raw_input("Wait for DUT to check for update, then press ENTER")

    #KEYCODE_DPAD_CENTER
    adbKeyCommand('23')
    adbTextCommand(DUT.userName)
    adbKeyCommand('61')
    adbTextCommand(DUT.userPass)
    adbKeyCommand('61')
    adbKeyCommand('23')
    #need to skip video somehow
    skipIntroVideo()

    adbKeyCommand('23')

    print "registered your DUT if nothing got F*cked up!"

main()
