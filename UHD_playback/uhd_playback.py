#Simple script to play UHD content on SLoane
import time, os, sys
import subprocess

def start_playback():
    asin = "B00OPX4UZE" #Alpha House 4K content

    playback = "adb shell "+"am start -n com.amazon.avod/.playbackclient.FOS5TvPlaybackActivity -e asin "+asin
    tv_model = raw_input("Enter TV Model (format: TV_MAKE): ")
    date = time.strftime("%Y-%m-%d-%H-%M")
    filename = "4K_"+tv_model+"_"+date
    logcmd = "(adb logcat -b amazon_main -b main | grep -i 'video quality change') > "+filename

    subprocess.Popen("adb kill-server", shell=True)
    time.sleep(5)
    subprocess.Popen("adb start-server", shell=True)
    time.sleep(5)

    subprocess.Popen("adb root", shell=True)
    time.sleep(3)
    subprocess.Popen("adb remount", shell=True)
    time.sleep(3)
    #force Sloane 4K @ 30Hz command
    print "switching to 4K on TV"
    subprocess.Popen("adb shell \"echo switch_res:23 > /d/hdmi\"", shell=True)
    print "sleep 10 sec until video comes back"
    time.sleep(10)
    print "starting playback & logs"
    subprocess.Popen(plaback, shell=True)
    subprocess.Popen(logcmd, shell=True)

    #suspend execution until timer elapses
    time.sleep(1800) # 30 minutes
    print "30 minutes up & killing logs"
    subprocess.Popen("kill-all adb", shell=True)

def checkIfDevice():
    adb = subprocess.Popen(["adb", "devices"], stdout=subprocess.PIPE)
    while adb.poll() is None:
        output = adb.stdout.readline()
        output = adb.stdout.readline()
        if "device" in output:
            if "device" in adb.stdout.readline():
                print "Multiple devices attached, exiting!"
                sys.exit(1)
            else:
                break
        else:
            print "No devices attached, exiting!"
            sys.exit(1)

    #one device at this point
    splitUp = output.split('\t')
    serialNo = splitUp[0]
    return serialNo

def main():
    checkIfDevice()
    start_playback()

main()
