batt=`dumpsys battery | grep "level"`
date=`date`

echo "Date  \t\t\t\t\tBattery" > batteryLevel.txt
echo "----  \t\t\t\t\t-------" >> batteryLevel.txt
echo "$date \t      $batt" >> batteryLevel.txt

while true; do
    echo "sleeping 30 minutes"
    sleep 1800
    echo "$date \t      $batt" >> batteryLevel.txt
done

