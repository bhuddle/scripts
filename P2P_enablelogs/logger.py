#!/usr/bin/python

#simple script for enabling logs on devices

import subprocess, sys, os, string, time
import re, datetime, getopt, glob
import threading, multiprocessing


def prepDevice(device_serialnumber, cCode):
    output = adbShellCommand(device_serialnumber, "getprop | grep build")
    devName = re.search('(?<=\[ro.build.product\]: \[).*(?=\])',output).group()
    if not displayDevice(devName):
        raise ValueError("Invalid Device: {0}".format(devName))
    print '['+devName+']:'+adbShellCommand(device_serialnumber, "root")
    time.sleep(1)
    print '['+devName+']:'+adbShellCommand(device_serialnumber, "remount")
    time.sleep(1)
    adbShellCommand(device_serialnumber, "logcat -c")
    time.sleep(1)

    if devName == "saturn" or devName == "tank":
        print '['+devName+']:'+"wpa_cli -i /data/misc/wifi/sockets/p2p-dev-wlan0 log_level DEBUG"
        output = adbShellCommand(device_serialnumber, "wpa_cli -i /data/misc/wifi/sockets/p2p-dev-wlan0 log_level DEBUG")
        print '['+devName+']:'+output
        if cCode != "":
            output = adbShellCommand(device_serialnumber, "wpa_cli -i /data/misc/wifi/sockets/wlan0 DRIVER COUNTRY "+cCode)
            print '['+devName+']:'+output
    else:
        print '['+devName+']:'+"wpa_cli -i /data/misc/wifi/sockets/p2p0 log_level DEBUG"
        output = adbShellCommand(device_serialnumber, "wpa_cli -i /data/misc/wifi/sockets/p2p0 log_level DEBUG")
        print '['+devName+']:'+output
        if cCode != "":
            output = adbShellCommand(device_serialnumber, "wpa_cli -i /data/misc/wifi/sockets/wlan0 DRIVER COUNTRY "+cCode)
            print '['+devName+']:'+output

    adbShellCommand(device_serialnumber, "rm -rf /data/system/dropbox")


def logcatStart(device,dev):
    output = adbShellCommand(device, "getprop | grep build")
    buildNo = re.search('(?<=\[ro.build.lab126.build\]: \[)\d+(?=\])',output).group()
    devName = re.search('(?<=\[ro.build.product\]: \[).*(?=\])',output).group()
    u = "_"
    if not displayDevice(devName):
        raise ValueError("Invalid Device: {0}".format(devName))
    print "kicking off logs as: "+devName+u+buildNo+u+dev+u+"Logcat.txt"
    print "Please Interrupt when test complete w/ (CTRL+C)"
    output = adbShellCommand(device, "logcat -v time > /sdcard/"+devName+u+buildNo+u+dev+u+"Logcat.txt")


def logcatPull(device, dev):
    output = adbShellCommand(device, "getprop | grep build")
    buildNo = re.search('(?<=\[ro.build.lab126.build\]: \[)\d+(?=\])',output).group()
    devName = re.search('(?<=\[ro.build.product\]: \[).*(?=\])',output).group()
    u = "_"
    print "pulling logs from /sdcard/"+devName+u+buildNo+u+dev+u+"Logcat.txt"
    output = adbShellCommand(device, "pull /sdcard/"+devName+u+buildNo+u+dev+u+"Logcat.txt")
    print output
    moveFileToDir(dev, devName+u+buildNo+u+dev+u+"Logcat.txt")


def getLogPull(device):
    ## pull getlogs with ./get_logs.sh -a -z -D <sn>
    output = subprocess.check_output(['./bin/get_logs.sh', 'GetLogs_for_'+device,'-a', '-Z', '-D', device])


def sniffCapture(dev, channel, width):
    if width in ('HT40+', 'HT40-'):
        wid = 'HT40'
    else: wid = width

    output = subprocess.check_output(['sh','./bin/cappy',channel,width,'./'+dev+'/Sniffer_'+channel+'_'+wid+'.pcapng'])

def getLogCleanUp(logDir):
    pwd = os.getcwd()
    listOfZip = glob.glob(pwd+"/*.zip")
    if not os.path.isdir(pwd+"/"+logDir):
        subprocess.check_output(['mkdir',logDir])
        for zFile in listOfZip:
            subprocess.check_output(['mv', zFile, logDir+'/'])
    else:
        for zFile in listOfZip:
            subprocess.check_output(['mv', zFile, logDir+'/'])

    time.sleep(5)
    #remove the directory left over
    listOfFolder = glob.glob(pwd+"/*GetLogs_for_*")
    for fold in listOfFolder:
        output = subprocess.check_output(['rm', '-r', fold])

    subprocess.check_output(['rm', 'ramdump_parser_skipped.txt'])


def checkIfDevice():
    listOfDevices = []
    output = subprocess.check_output(['adb', 'devices'])
    for line in output.split():
        if len(line) == 16:
            listOfDevices.append(line)

    return listOfDevices


def adbShellCommand(device, cmd):
    tmp = cmd.split()
    if cmd == "root" or cmd == "remount" or tmp[0] == "pull":
        output = subprocess.check_output(['adb','-s', device]+tmp)
    else:
        output = subprocess.check_output(['adb','-s', device, 'shell']+tmp)
    time.sleep(1)
    return output


def displayDevice(device):
    if device in ["thebes", "sloane", "montoya", "bueller", "tank", "saturn"]:
        return True
    else:
        print "*********************************************************"
        print "\t\tDEVICE TYPE OF {0} NOT DETECTED".format(device)
        print "*********************************************************"
        return False


def moveFileToDir(device, filename):
    pwd = os.getcwd()
    if not os.path.isdir(pwd+"/"+device):
        subprocess.check_output(['mkdir',device])
        subprocess.check_output(['mv', filename, device+'/'])
    else:
        if os.path.exists(pwd+"/"+device+"/"+filename):
            print "duplicate file found, adding time stamp!"
            date = datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
            f2 = filename[:-4]+date+'.txt'
            subprocess.check_output(['mv', filename, f2])
            subprocess.check_output(['mv', f2, device+'/'])
        else:
            subprocess.check_output(['mv', filename, device+'/'])


def main(argv):
    dev = ""
    cc = ""
    chan = ""
    wid = ""

    try:
        opts, args = getopt.getopt(argv,"ho:c:s:w:")
    except getopt.GetoptError:
        print 'sudo python logger.py'
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print '\nUsage: $sudo python logger.py\n'
            print '###running with no arguments will just run logger & name file based on DUT\'s connected'
            print '###include up to date get_logs.sh in bin & cappy###'
            print 'OPTIONS'
            print '-o <test_device>\tSpecify the device being tested i.e. Samsung_UN46000'
            print '-c <country_code>\tSpecify the country code to set it to i.e. JP or UK or FR'
            print '-s <channel>\t\tSpecifies that you would like the sniffer ran on a specific channel'
            print '-w <width>\t\tSpecifies the bandwidth i.e. (HT20, HT40-, HT40+)'
            sys.exit()
        elif opt == '-o':
            dev = arg
        elif opt == '-c':
            cc = arg
        elif opt == '-s':
            chan = arg
        elif opt == '-w':
            wid = arg

    if dev == "":
        dev = "Log"

    sn = checkIfDevice()
    if not sn:
        print "please connect a device to ADB"
        sys.exit(0)

    if (chan != "" and wid == "") or (chan == "" and wid != ""):
        print "Provide both channel and width"
        sys.exit()

    pwd = os.getcwd()
    if not os.path.isdir(pwd+"/"+dev):
        subprocess.check_output(['mkdir',dev])

#### Want to start threading multiple devices here
    threads = []

    for device in sn:
        devThread = threading.Thread(target=prepDevice, args=(device,cc,))
        threads.append(devThread)
        devThread.start()

    for thread in threads:
        thread.join()

    time.sleep(5)
    print "Setup complete on all attached devices"
    raw_input("Press Enter to continue")

    output = adbShellCommand(device, "getprop | grep build")
    buildNo = re.search('(?<=\[ro.build.lab126.build\]: \[)\d+(?=\])',output).group()
    devName = re.search('(?<=\[ro.build.product\]: \[).*(?=\])',output).group()
    u = "_"

#### kicking off logcat until there is a CTRL+C
    processes = []
    for device in sn:
        devProcess = multiprocessing.Process(target=logcatStart, args=(device, dev,))
        processes.append(devProcess)
        devProcess.start()

    print "sniffer starting"
    sniffProcess = multiprocessing.Process(target=sniffCapture, args=(dev, chan, wid,))
    processes.append(sniffProcess)
    sniffProcess.start()

    try:
        while devProcess.is_alive(): time.sleep(1)
    except KeyboardInterrupt:
        print "\nKeyboard Interrupt (CTRL+C) -- Pulling logcat logs"
        for proc in processes:
            proc.terminate()

#### pulling logcat for devices attached
    for device in sn:
        devProcess = multiprocessing.Process(target=logcatPull, args=(device, dev,))
        processes.append(devProcess)
        devProcess.start()

#### pulling getlogs for attached devices
    for device in sn:
        devThread = threading.Thread(target=getLogPull, args=(device,))
        threads.append(devThread)
        devThread.start()

    print "Pulling get_logs.sh ... Please be patient"
    for thread in threads:
        thread.join()

#### clean up the directory of misc folders
    getLogCleanUp(dev)
    print "files can be found under {0}".format(dev)


if __name__ == "__main__":
    main(sys.argv[1:])
