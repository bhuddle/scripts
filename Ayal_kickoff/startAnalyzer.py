""" Wrapper file to continuously analyze large data for Ayal's Audio Analysis"""
""" Designed by Benjamin Huddle, AllionUSA"""

import subprocess
import time
import os

""" put in base directory of the file system """

print "-----------------------------"
print "Starting Analysis for Results"
print "-----------------------------"

time.sleep(5)

path=os.getcwd()
print "-----------------------------"
print "\n\nstarting to analyze data from this directory: " + path
execute = "allionGlitchTest <WavFileName> -f -W 14"
print "current command to evaluate data: $" + execute
print "-----------------------------"
cmd = execute.split()

list_of_files = {}
for (dirpath, dirnames, filenames) in os.walk(path):
    for filename in filenames:
        if filename.endswith('.wav'):
            print "\n\n-----------------------------"
            print "Running Analysis on "+ filename
            print "-----------------------------"
            os.chdir(dirpath)
            cmd[1] = filename
            subprocess.check_output(cmd)

print "-----------------------------"
print "Analysis for results complete"
print "-----------------------------"
