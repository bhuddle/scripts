import subprocess, time, logging, os, glob

class GetLogs(object):
    def __init__(self, device, directory):
        super(GetLogs, self).__init__()
        self._dev = device      #device serial
        self._dir = directory   #directory to send it to

    def RunGetLogs(self):
        pwd = os.getcwd()
        subprocess.check_output(['./bin/get_logs.sh', 'GetLogs_for_'+self._dev,'-a', '-Z', '-D', self._dev])
        listOfZip = glob.glob(pwd+"/*.zip")
        if os.path.isdir(self._dir):
            for zFile in listOfZip:
                subprocess.check_output(['mv', zFile, self._dir])
        elif not os.path.isdir(self._dir):
            subprocess.check_output(['mkdir', self._dir])
            for zFile in listOfZip:
                subprocess.check_output(['mv', zFile, self._dir])
        time.sleep(5)
        listOfFolder = glob.glob(pwd+"/*GetLogs_for_*")
        for fold in listOfFolder:
            output = subprocess.check_output(['rm', '-r', fold])

        try: subprocess.check_output(['rm', 'ramdump_parser_skipped.txt'])
        except: pass


"""gl = GetLogs("7094160945131NGN", "Log_Results/Results_12:00:46 01-Aug-2016")
print "Running get logs"
gl.RunGetLogs()
print "finished"""
