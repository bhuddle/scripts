import subprocess, logging
import time
import AndroidDUT

class DumpsysCheck(object):
    def __init__(self):
        self._status = {}
        self._client = ''

    """Parse string value into dict for pass/fail criteria"""
    def ParseDumpsys(self, value, client):
        dumpsys = value.split('\r')
        if client == "source":
            criteria = {'curState':'', 'groupFormed':'', 'WFD enabled':'', 'state':'', 'groupOwnerAddress': '', 'total records': ''}
            for text in dumpsys:
                if "curState" in text:
                    criteria['curState'] = text.split('=')[1]
                if "groupFormed:" in text:
                    tmp = text.split()
                    criteria['groupFormed'] = tmp[tmp.index('groupFormed'+":") + 1]
                if "WFD enabled:" in text:
                    tmp = text.split()
                    criteria['WFD enabled'] = tmp[tmp.index("enabled:") + 1]
                if "state:" in text:
                    tmp = text.split()
                    tmp = tmp[tmp.index("state:") + 1]
                    criteria['state'] = tmp[:-1]
                if "groupOwnerAddress:" in text:
                    tmp = text.split('groupOwnerAddress:')[1]
                    criteria['groupOwnerAddress'] = tmp.lstrip()
                    if criteria['groupOwnerAddress'] != 'null':
                        criteria['groupOwnerAddress'] = criteria['groupOwnerAddress'].lstrip('/')
                if "total records" in text:
                    tmp = text.split('=')[1]
                    criteria['total records'] = str(tmp)
            self._status = criteria
            return self._status
        elif client in "sink":
            #need to check if the device is added in dumpsys correctly.
            criteria = {'deviceAddress':'', 'total records':''}
            tmp = ''
            nextAddr = False
            for text in dumpsys:
                if "total records" in text:
                    temp = text.split('=')[1]
                    criteria['total records'] = str(temp)
                if "Client: Device:" in text:
                    if nextAddr == False and "Gelato" not in text:
                        nextAddr = True
                if "deviceAddress" in text and nextAddr == True:
                    tmp = text.split()[1]
                    nextAddr = False
            criteria['deviceAddress'] = tmp
            self._status = criteria
            return self._status

    def CheckDumpsys(self, crit, client):
        #checking after an attempt to connect.  Should pass, otherwise all results are fail to connect
        if client == "source":
            if crit['curState'] == "GroupCreatedFinalState" and crit['groupFormed'] == 'true' and crit['WFD enabled'] == 'true' and crit['state'] == "CONNECTED/CONNECTED":
                crit['result'] = "PASS"
                return crit
            # for some reason this state seems unstable but I guess could be passing
            elif crit['curState'] == "OngoingGroupRemovalState" and crit['groupFormed'] == 'true' and crit['WFD enabled'] == 'true' and crit['state'] == "CONNECTED/CONNECTED":
                crit['result'] = "PASS"
                return crit
            elif crit['curState'] == "GroupCreatedFinalState" and crit['groupFormed'] == 'false' and crit['WFD enabled'] == 'true' and crit['state'] == "CONNECTED/CONNECTED":
                crit['result'] = "PASS - groupFormed = "+crit['groupFormed']
                return crit
            elif crit['curState'] == "InactiveState" and crit['state'] == "DISCONNECTED/FAILED":
                crit['result'] = "FAIL - state = "+crit['state']
                return crit
            elif crit['curState'] == "InactiveState" and crit['state'] == "DISCONNECTED/DISCONNECTED":
                crit['result'] = "FAIL - state = "+crit['state']
                return crit
            elif crit['curState'] == "InactiveState" and crit['state'] == "UNKNOWN/IDLE":
                crit['result'] = "FAIL - state = "+crit['state']
                return crit
            else:
                crit['result'] = "FAIL - state = "+crit['state']
                return crit
        elif client == "sink":
            #appears as if sink retains device address after disconnection (not pass fail criteria)
            if crit['deviceAddress'] != '':
                crit['result'] = "PASS"
                return crit
            else:
                crit['result'] = "FAIL - DEVICE ADDRESS NOT FOUND"
                return crit


    def CheckRecords(self, prevRecord, dump):
        dumpsys = dump.split('\r')
        i = int(prevRecord)
        retStr = ''
        #if both previous record # is the same as current record -> print last message
        if "total records" in dumpsys[1]:
            temp = dumpsys[1].split('=')[1]
            if i == int(temp):
                for text in dumpsys:
                    if "rec["+str(i-1)+"]" in text:
                        retStr += text
                        return retStr
        #otherwise print the difference between records
        for text in dumpsys:
            if "rec["+str(i)+"]" in text:
                retStr += text
                i += 1

        return retStr

    def CheckAudio(self, sink_dut):
        dump = sink_dut.RunCommand("dumpsys audio").WaitForComplete()
        dumpsys = dump.split('\r')
        audioString = ""
        for text in dumpsys:
            if "source" in text:
                audioString = text
                break
        if audioString != "":
            result = "CONNECTED"
        else:
            result = "DISCONNECTED"
        return result

    def CheckConnection(self, source_dut):
        dump = source_dut.RunCommand("dumpsys wifip2p").WaitForComplete()
        parsed = self.ParseDumpsys(dump, "source")
        checked = self.CheckDumpsys(parsed, "source")
        if parsed['groupOwnerAddress'] != 'null' or parsed['groupOwnerAddress'] != '':
            pinger = source_dut.RunCommand("ping -c 5 "+parsed['groupOwnerAddress']).WaitForComplete()
        else:
            pinger = 'null'
        if checked['result'] == "PASS" and 'time=' in pinger:
            return "CONNECTED"
        else:
            print "DUT is not connected"
            print checked['result']
            return "DISCONNECTED"

    def CheckVideo(self, source_dut):
        dump = source_dut.RunCommand("dumpsys media_session").WaitForComplete()
        dumpsys = dump.split('\r')
        for text in dumpsys:
            if "com.amazon.avod" in text:
                dump = "CONNECTED"
                break
        if dump == "CONNECTED":
            return "PLAYING"
        else:
            return "IS NOT PLAYING"



"""sourcedumpsys = subprocess.check_output(['adb', '-s', 'G000K10460460NEJ', 'shell', 'dumpsys', 'wifip2p'])
sinkdumpsys = subprocess.check_output(['adb', '-s', '7094160945131NGN', 'shell', 'dumpsys', 'wifip2p'])
parsedSourceCriteria = DumpsysCheck().ParseDumpsys(sourcedumpsys, "source")
parsedSinkCriteria = DumpsysCheck().ParseDumpsys(sinkdumpsys, "sink")
print parsedSourceCriteria['groupOwnerAddress']
print parsedSinkCriteria
time.sleep(5)
sourceValue = DumpsysCheck().CheckDumpsys(parsedSourceCriteria, "source")
sinkValue = DumpsysCheck().CheckDumpsys(parsedSinkCriteria, "sink")
print sourceValue['result']
print sinkValue['result']

poo = DumpsysCheck().CheckRecords("2", sourcedumpsys)
print poo"""
