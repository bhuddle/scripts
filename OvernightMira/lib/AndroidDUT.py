import time, subprocess, re
import PollingObject

class AndroidDUT(object):
    def __init__(self, serial):
        super(AndroidDUT,self).__init__()
        self._ser = serial      #serial number
        self._dev = None        #bueller, tank, montoya, etc
        self._build = None      #build of device
        self._type = None       #sink or source

    #preps the duts
    def Prep(self):
        output = self.RunCommand('getprop | grep build').WaitForComplete()
        self._dev = re.search('(?<=\[ro.build.product\]: \[).*(?=\])',output).group()
        if self._dev in ('bueller','montoya','sloane','tank'):
            self._type = 'sink'
        else:
            self._type = 'source'
        self._build = re.search('(?<=\[ro.build.version.number\]: \[).*(?=\])',output).group()
        print self._dev+' '+self.RunCommand("root").WaitForComplete()
        print self._dev+' '+self.RunCommand("remount").WaitForComplete()

        #enable verbose logging and all that shiz
        if self._dev in ('saturn','tank'):
            cmd = 'wpa_cli -i /data/misc/wifi/sockets/p2p-dev-wlan0 log_level DEBUG'
            print self._dev+' '+cmd
            print self.RunCommand(cmd).WaitForComplete()
        else:
            cmd = 'wpa_cli -i /data/misc/wifi/sockets/p2p0 log_level DEBUG'
            print self._dev+' '+cmd
            print self.RunCommand(cmd).WaitForComplete()

    #runs specific command prepended with 'adb -s <device> shell'
    def RunCommand(self, command):
        cmd = command.split()
        if cmd[0] in ("root", "remount"):
            tmp = ['adb', '-s', self._ser]+cmd
            #out = subprocess.check_output(['adb','-s', self._ser]+cmd)
        else:
            tmp = ['adb', '-s', self._ser, 'shell']+cmd
            #out = subprocess.check_output(['adb','-s', self._ser, 'shell']+cmd)

        try:
            self._process = subprocess.Popen(tmp,stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
        except:
            time.sleep(1)
            return self.RunCommand(tmp)
        return PollingObject.PollingObject(self._process)

"""dut = AndroidDUT('7094160945131NGN')
dut.Prep()
"""
