import select
import time
class PollingObject(object):
    def __init__(self, popen_object, timeout=None):
        self._popen_object = popen_object
        self._timeout = timeout

    def WaitForComplete(self):
        data = ''
        poll_obj = select.poll()
        poll_obj.register(self._popen_object.stdout, select.POLLIN)
        
        if self._timeout is not None:
            end_time = time.time() + self._timeout
            while time.time() < end_time:
                poll_result = poll_obj.poll(0)
                if poll_result:
                    if poll_result[0][1] is not 16:
                        data += self._popen_object.stdout.readline()
                    else:
                        break
            return data
        
        else:
            while True:
                poll_result = poll_obj.poll(0)
                if poll_result:
                    if poll_result[0][1] is not 16:
                        data += self._popen_object.stdout.readline()
                    else:
                        break
            return data


    def LiveData(self):
        return self._popen_object
    
    
    """
    stop_phrase is a dictionary formatted as such: {'Info':[phrase_to_stop_on, kill_function, parameter_for_kill function(process name)]}
    """
    def QueueLiveData(self, queue, grep_parameters=None, stop_phrase=None):
        poll_obj = select.poll()
        poll_obj.register(self._popen_object.stdout, select.POLLIN)
        data = ''
        end_time = time.time() + self._timeout
        time.sleep(3)
        while time.time() < end_time:
            poll_result = poll_obj.poll(0)
            if poll_result:
                if poll_result[0][1] is not 16:
                    while '\n' not in data:
                        data += self._popen_object.stdout.read(1)
                    queue.put(data)
                    data = ''
                else:
                    break
