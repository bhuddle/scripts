# Script to keep track of overnight status, count and logs in case of failure
# Questions, comments, concerns email: BenHuddle@allionusa.com

import sys, os, subprocess, time, logging, threading
from lib.AndroidDUT import AndroidDUT
from lib.GetLogs import GetLogs
from lib.DumpsysCheck import DumpsysCheck


def checkForDevice():
    listOfDevices = []
    out = subprocess.check_output(['adb', 'devices'])
    for line in out.split():
        if len(line) == 16:
            listOfDevices.append(line)

    return listOfDevices

def main(argv):
    activity = 'am start -n com.amazon.avod/.playbackclient.FireOS5TabletPlaybackActivity --es asin B00KIGRRRU --el Timecode 0 --activity-clear-top'
    sleep_time = 130 #7980
    count = 0
    dc = DumpsysCheck()

    serials = checkForDevice()
    if not serials:
        print "Please connect a device to ADB"
        sys.exit(0)

    print "\n\n\nPlease make sure Miracast session is established and connected to correct account"
    print "Correct account: miracast2@allionusa.com"
    print "Password: wfd12345\n\n\n"

    list_of_duts = []
    list_of_logs = []

    for device in serials:
        dut = AndroidDUT(device)
        logs = GetLogs(device, "Logs")
        list_of_duts.append(dut)
        list_of_logs.append(logs)

    for dut in list_of_duts:
        dut.Prep()
        if dut._type == 'source':
            source_dut = dut
        else:
            sink_dut = dut

    conn = dc.CheckConnection(source_dut)
    audio = dc.CheckAudio(sink_dut)
    if conn != 'CONNECTED' and audio != 'CONNECTED':
        print 'Please connect both sink and source devices before begining test.'
        sys.exit(0)

    while True:
        cur_time = 0
        test_pass = False
        source_dut.RunCommand(activity).WaitForComplete()
        time.sleep(5)
        cur_time = 5
        #Add logic to check every 2 minutes for a failure
        while cur_time < sleep_time:
            audio = dc.CheckAudio(sink_dut)
            video = dc.CheckVideo(source_dut)
            conn = dc.CheckConnection(source_dut)
            if cur_time < sleep_time and audio == 'CONNECTED' and video == 'PLAYING' and conn == 'CONNECTED':
                print 'still connected'
                time.sleep(60)
                cur_time = cur_time + 70
            else:
                print 'test failed'
                print 'audio status: '+audio
                print 'video status: '+video
                print 'conn status: '+conn
                threads = []
                for log in list_of_logs:
                    glThread = threading.Thread(target=log.RunGetLogs)
                    threads.append(glThread)
                    glThread.start()
                for thread in threads:
                    thread.join()
                break

            if cur_time >= sleep_time and audio == 'CONNECTED' and video == 'PLAYING' and conn == 'CONNECTED':
                print 'test passed'
                test_pass = True
                break

        if test_pass:
            count = count + 1
            print 'successful iterations: '+str(count)
        #sleep for a bit before next iteration
        time.sleep(10)

if __name__ in "__main__":
    main(sys.argv[1:])
