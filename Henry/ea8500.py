"""
Firmware: 1.0.99.168206
"""
import requests
import time
import urllib
import os
import AccessPoint
import json

class ea8500(AccessPoint.AccessPoint):
    def __init__(self, params):
        super(ea8500, self).__init__(params)
        self.values = {"channel":["1","2","3","4","5","6","7","8","9","10","11"],
        "modes":[{"bgn":"802.11mixed"}, {"g":"802.11g"}, {"n":"802.11n"},{"bg":"802.11bg"}],
        "channelwidth":[{"20":"Standard"}, {"20/40":"Auto"}],
        "securities":["WPA2","WEP","Open","WPA2Enterprise"]}
        self.values5 = {"channel":["36","40","44","48","149","153","157","161"],
        'securities':["WPA2","WEP","Open","WPA2Enterprise"],
        "modes":[{"anac":"802.11mixed"}, {"ac":"802.11ac"}, {"n":"802.11n"},{"a":"802.11a"},{"an":"802.11an"}],
        "channelwidth":[{"20":"Standard"}, {"20/40":"Auto"}, {"40":"Wide"}, {"80":"Wide80"}]}
        self._url = "http://"+self.ip_address+"/JNAP/"
        self._session = requests.Session()
        self._broadcast = None
#        self._broadcast = True
        self._channel_width = None
        self._mode = None
        
    def __SetChannelWidth(self, value):
        mode = self.values.get("channelwidth")
        for d in mode:
            if value in d:
                return d.get(value)
            
    def __SetChannelWidth5(self, value):
        mode = self.values5.get("channelwidth")
        for d in mode:
            if value in d:
                return d.get(value)

    def SetChannelWidth(self, value):
        if self.Band == "2.4":
            self._channel_width = unicode(self.__SetChannelWidth(value))
        elif self.Band == "5":
            self._channel_width = unicode(self.__SetChannelWidth5(value))

    def SetChannel(self, value):
        self._channel = int(value)

    def SetBroadcast(self, value):
        self._broadcast = value

    def SetMode(self, value):
        if self.Band == "2.4":
            self._mode = unicode(self.__SetMode(value))
        elif self.Band == "5":
            self._mode = unicode(self.__SetMode5(value))

    def SetSecurity(self, value):
        if self.Band == "2.4":
            if value == "WPA2":
                self._security = "WPA2"
            elif value == "WEP":
                self._security = "WEP"
            elif value == "Open":
                self._security = "Open"
            elif value == "WPA2Enterprise":
                self._security = "WPA2Enterprise"
        elif self.Band == "5":
            if value == "WPA2":
                self._security = "WPA2"
            elif value == "WEP":
                self._security = "WEP"
            elif value == "Open":
                self._security = "Open"
            elif value == "WPA2Enterprise":
                self._security = "WPA2Enterprise"
                
    def Post(self):
        self.__FIVE_POST()
        time.sleep(10)
            
    def __SetMode(self, value):
        u = self.values.get("modes")
        for d in u: 
            if value in d:
                return d.get(value)

    def __SetMode5(self, value):
        u = self.values5.get("modes")
        for d in u:
            if value in d:
                return d.get(value)
            

    def __UPDATE_JSON(self, value):
        if self.Band == "2.4":
            value = [{u'action': u'http://linksys.com/jnap/wirelessap/SetRadioSettings', u'request': {u'radios': [{u'radioID': u'RADIO_2.4GHz', u'settings': {u'broadcastSSID': self._broadcast, u'channelWidth': self._channel_width, u'ssid': self.SSID, u'isEnabled': True, u'mode': self._mode, u'channel': self._channel}}, {u'radioID': u'RADIO_5GHz', u'settings': {u'broadcastSSID': True, u'channelWidth': u'Wide', u'ssid': u'ea8500_5', u'wpaPersonalSettings': {u'passphrase': u'11111111'}, u'isEnabled': True, u'mode': u'802.11mixed', u'security': u'WPA2-Personal', u'channel': 161}}]}}]

            if self._security == "Open":
                value[0]['request']['radios'][0]['settings'] = {"isEnabled":True,"mode":self._mode,"ssid":self.SSID,"broadcastSSID":self._broadcast,"channelWidth":self._channel_width,"channel":self._channel,"security":"None"}
            elif self._security == "WEP":
                value[0]['request']['radios'][0]['settings'] = {"isEnabled":True,"mode":self._mode,"ssid":self.SSID,"broadcastSSID":self._broadcast,"channelWidth":self._channel_width,"channel":self._channel,"security":"WEP","wepSettings":{"encryption":"WEP-64","key1":self.AuthenticationKey,"key2":"","key3":"","key4":"","txKey":1}}
            elif self._security == "WPA2":
                value[0]['request']['radios'][0]['settings'] = {"isEnabled":True,"mode":self._mode,"ssid":self.SSID,"broadcastSSID":self._broadcast,"channelWidth":self._channel_width,"channel":self._channel,"security":"WPA2-Personal","wpaPersonalSettings":{"passphrase":self.AuthenticationKey}}
            elif self._security == "WPA2Enterprise":
                value[0]['request']['radios'][0]['settings'] = {"isEnabled":True,"mode":self._mode,"ssid":self.SSID,"broadcastSSID":self._broadcast,"channelWidth":self._channel_width,"channel":self._channel,"security":"WPA2-Enterprise","wpaEnterpriseSettings":{"radiusServer":self.RadiusIP,"radiusPort":int(self.RadiusPort),"sharedKey":self.RadiusKey}}

        elif self.Band == "5":
            value = [{u'action': u'http://linksys.com/jnap/wirelessap/SetRadioSettings', u'request': {u'radios': [{u'radioID': u'RADIO_2.4GHz', u'settings': {u'broadcastSSID': True, u'channelWidth': u'Auto', u'ssid': u'ea8500_24', u'wpaPersonalSettings': {u'passphrase': u'11111111'}, u'isEnabled': True, u'mode': u'802.11mixed', u'security': u'WPA2-Personal', u'channel': 7}}, {u'radioID': u'RADIO_5GHz', u'settings': {u'broadcastSSID': self._broadcast, u'channelWidth': self._channel_width, u'ssid': self.SSID, u'isEnabled': True, u'mode': self._mode, u'channel': self._channel}}]}}]

            if self._security == "Open":
                value[0]['request']['radios'][1]['settings'] = {"isEnabled":True,"mode":self._mode,"ssid":self.SSID,"broadcastSSID":self._broadcast,"channelWidth":self._channel_width,"channel":self._channel,"security":"None"}
            elif self._security == "WEP":
                value[0]['request']['radios'][1]['settings'] = {"isEnabled":True,"mode":self._mode,"ssid":self.SSID,"broadcastSSID":self._broadcast,"channelWidth":self._channel_width,"channel":self._channel,"security":"WEP","wepSettings":{"encryption":"WEP-64","key1":self.AuthenticationKey,"key2":"","key3":"","key4":"","txKey":1}}
            elif self._security == "WPA2":
                value[0]['request']['radios'][1]['settings'] = {"isEnabled":True,"mode":self._mode,"ssid":self.SSID,"broadcastSSID":self._broadcast,"channelWidth":self._channel_width,"channel":self._channel,"security":"WPA2-Personal","wpaPersonalSettings":{"passphrase":self.AuthenticationKey}}
            elif self._security == "WPA2Enterprise":
                value[0]['request']['radios'][1]['settings'] = {"isEnabled":True,"mode":self._mode,"ssid":self.SSID,"broadcastSSID":self._broadcast,"channelWidth":self._channel_width,"channel":self._channel,"security":"WPA2-Enterprise","wpaEnterpriseSettings":{"radiusServer":self.RadiusIP,"radiusPort":int(self.RadiusPort),"sharedKey":self.RadiusKey}}

        return value


    def __FIVE_POST(self):
        formatted_data = []
        formatted_data = self.__UPDATE_JSON(formatted_data)
        
        header={"Host":self.ip_address,
        "User-Agent":"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0",
        "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language":"en-US,en;q=0.5",
        "Accept-Encoding":"gzip, deflate",
        "Referer":"http://"+self.ip_address+"/ui/1.0.99.168206/dynamic/home.html",
        "X-JNAP-Action":"http://linksys.com/jnap/core/Transaction",
        "X-JNAP-Authorization":"Basic YWRtaW46YWRtaW4=",
        "Cookie":"is_cookies_enabled=enabled; admin-auth=Basic%20YWRtaW46YWRtaW4%3D; current-applet=7B1F462B-1A78-4AF6-8FBB-0C221703BEA4; initial-tab=",
        "Connection":"keep-alive"}
        
        z = self._session.post(self._url, data=json.dumps(formatted_data), headers=header, auth=('admin','admin'))


"""f = ea8500(["../../Routers/ea8500_5GHz.ini", "abc", "def", "hij"])
f.SetMode("ac")
f.SetSecurity("WPA2")
f.SetChannel("161")
f.SetBroadcast(True)
f.SetChannelWidth("80")
f.Post()
"""
