# Written by Franklin Harrison, Allion USA
# Modified by Benjamin Huddle, Allion USA
import subprocess as sp
import urllib, urllib2, re
from time import sleep, time
import configobj
import os


class Socket(object):
    """
    | ip_address = String that represents the IP Address of the NAXX Strip
    | socket_number = integer that represents which power socket on the strip it is
    """
    def __init__(self, ip_address, socket_number=None):
        self._socket_number = socket_number
        self._urllib = urllib.URLopener()
        self._ipaddress = ip_address
        self._binded_object = []
        

    def On(self):
        """
        Allows the individual socket to be turned on
        """
        sleep(1)
        try:
            open_response = self._urllib.open("http://admin:admin@" + str(self._ipaddress) + "/cmd.cgi?$A3 " + str(self._socket_number) +  ' 1')
        except IOError,e:
            raise ValueError("Before coming to Frank: Make sure NAXX Strip Ethernet Cable is tight, and that the Realtek P8H77 Ethernet Interface is set to: Internet And 20\n If this does not fix it, make sure the NAXX Strip IP Address matches the IP Address in NaxxConfigx.ini, located in ConfigFiles folder in the previous directory.")

            
    def Off(self):
        """
        Allows the individual socket to be turned off
        """
        sleep(1)
        try:
            open_response = self._urllib.open("http://admin:admin@" + str(self._ipaddress) + "/cmd.cgi?$A3 " + str(self._socket_number) +  ' 0')
        except IOError,e:
            raise ValueError("Before coming to Frank: Make sure NAXX Strip Ethernet Cable is tight, and that the Realtek P8H77 Ethernet Interface is set to: Internet And 20\n If this does not fix it, make sure the NAXX Strip IP Address matches the IP Address in NaxxConfigx.ini, located in ConfigFiles folder in the previous directory.")
        
    def __str__(self):
        return "Socket"+str(self._socket_number)

    def Ping(self, ap_ip, wait_for_complete=False):
        """
        | Pings to check if the AP is finished loading
        | ap_ip: string - ap's ip address(or any object that contains an IP address)
        | returns: True if successful, False if false
        """
        end_time = time() + 300
        if wait_for_complete:
            data = sp.Popen(['ping','-c','1',ap_ip], stdout = sp.PIPE, stderr=sp.PIPE).communicate()[0]
            while "time=" not in data and time() < end_time:
                data = sp.Popen(['ping','-c','1',ap_ip], stdout = sp.PIPE, stderr=sp.PIPE).communicate()[0]
            if time() > end_time:
                return False
            else:
                sleep(10)
                return True
        else:
            proc = sp.Popen(['ping','-c','1',ap_ip], stdout = sp.PIPE, stderr=sp.PIPE)
            return proc.communicate()[0]
    
    def Bind(self, obj):
        """
        | Binds an object to the socket - the object must implement a Run function
        | obj = the item you want to bind
        """
        self._binded_object.append(obj)
    
    @property
    def BindedObject(self):
        """
        returns the binded object
        """
        return self._binded_object
    
class NAXXStrip(Socket):
    def __init__(self, ip_address, sockets, path):
        """
        ip_address = string - IP Address Of The NAXX Strip
        socket = integer - the number of sockets
        path = string - the path to the NAXXConfig's .ini file
        """
        self._ipaddress = ip_address
        self._sockets = []
        self._path = path
        
        #NAXX Strips use 1 to start the index. Python lists use 0 to start the index.
        for i in range(1, int(sockets) + 1):
            self._sockets.append(Socket(ip_address, i))
        Socket.__init__(self, ip_address=self._ipaddress)


    @property
    def Path(self):
        """
        returns path to ini file
        """
        return self._path
        
    @property
    def IPAddress(self):
        """
        returns naxx strip's ip address
        """
        return self._ipaddress
    
    @property
    def Sockets(self):
        """
        returns number of sockets
        """
        return self._sockets
    
    def GetSocket(self, socket_number):
        """
        socket_number: integer - the socket number you want
        returns a socket object
        """
        for index, socket in enumerate(self._sockets):
            if index + 1 == socket_number:
                return socket
    
    def TurnOn(self):
        """
        Turns on all sockets that the NAXX Strip holds
        """
        for index,socket in enumerate(self._sockets):
            sleep(1)
            socket.On()
    
    def TurnOff(self):
        """
        Turns off all socket that the NAXX Strip holds
        """
        for index,socket in enumerate(self._sockets):
            sleep(1)
            socket.Off()
            

    def pinger(self):
        find = None
        time = '1'
        ping = 'ping -c'+time+' '+self._ipaddress
        p = ping.split()
        try:
            out = sp.check_output(p)
            find = re.search(time+'\ received',out).group()
        except sp.CalledProcessError:
            find = None

        if find != None:
            return True
        else:
            return False




    def __str__(self):
        return "Remote IP Strip - IP Address: " + str(self._ipaddress)

    @staticmethod
    def GetNaxxStrips(config_path, config_name, config_section, element):
        """
        | config_path - string formatted path, with path localized to the current directory this file is in
        | config_name - the name of the configuration file
        | config_section - the primary header
        | element - the key that exists within the header
        | returns naxx strip(s) objects
        """
        list_of_strips = []
        naxx_strips = configobj.ConfigObj(os.path.join(os.path.dirname(os.path.abspath(__file__)),config_path+config_name))[config_section][element]
        if isinstance(naxx_strips, list):
            for naxx_strip in naxx_strips:
                naxx = NAXXStrip(configobj.ConfigObj(config_path+naxx_strip+'.ini')["status"]["ip_address"], configobj.ConfigObj(config_path+naxx_strip+'.ini')["status"]["sockets"], config_path+naxx_strip+'.ini')
                list_of_strips.append(naxx)
        else:
            naxx = NAXXStrip(configobj.ConfigObj(config_path+naxx_strips+'.ini')["status"]["ip_address"], configobj.ConfigObj(config_path+naxx_strips+'.ini')["status"]["sockets"], config_path+naxx_strips+'.ini')
            list_of_strips.append(naxx)
        return list_of_strips
