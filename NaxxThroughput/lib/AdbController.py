# Created by Benjamin Huddle, Allion USA
# Will handle enumerating ADB devices into a list, then other basic commands
import subprocess, sys, re, time

def checkIfDevice():
    listOfDevices = []
    output = subprocess.check_output(['adb', 'devices'])
    for line in output.split():
        if len(line) == 16:
            listOfDevices.append(line)
        elif ":" in line:
            listOfDevices.append(line)

    return listOfDevices


def adbShellCommand(device, cmd):
    tmp = cmd.split()
    if cmd == "root" or cmd == "remount" or tmp[0] == "pull":
        output = subprocess.check_output(['adb','-s', device]+tmp)
    else:
        output = subprocess.check_output(['adb','-s', device, 'shell']+tmp)
    time.sleep(1)
    return output


def adbConnect(ipaddr):
    cmd = "adb connect "+ipaddr
    output = subprocess.check_output(cmd.split())
    if "unable" not in output:
        out = re.search('(\d+\.\d+\.\d+\.\d+:\d+)',output).group()
        return out
    else:
        return None


def adbDisconnect():
    cmd = "adb disconnect"
    subprocess.check_output(cmd.split())


def displayDevice(device):
    if device in ["thebes", "sloane", "montoya", "bueller", "tank", "saturn"]:
        return True
    else:
        print "*********************************************************"
        print "\t\tDEVICE TYPE OF {0} NOT DETECTED".format(device)
        print "*********************************************************"
        return False


def restartServer():
    cmd = "adb kill-server"
    cmd2 = "adb start-server"
    subprocess.check_output(cmd.split())
    time.sleep(2)
    subprocess.check_output(cmd2.split())
    time.sleep(2)

def pinger(ipaddr):
    find = None
    time = '1'
    ping = 'ping -c'+time+' '+ipaddr
    p = ping.split()
    try:
        out = subprocess.check_output(p)
        find = re.search(time+'\ received',out).group()
    except subprocess.CalledProcessError:
        find = None

    if find != None:
        return True
    else:
        return False

