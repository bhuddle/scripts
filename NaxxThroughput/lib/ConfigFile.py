# Created by Benjamin Huddle, Allion USA
# Will handle generating config file and parsing / pulling data from config file
import ConfigParser


### Creates a default .ini file
def CreateConfig(cfg):
    cfgfile = open('./config/default_config.ini', 'w')
    cfg.add_section('NaxxOutlet')
    for i in range(1,9):
        cfg.set('NaxxOutlet',str(i),'AP_SSID'+str(i))
    cfg.add_section('Throughput')
    cfg.set('Throughput', 'time','300')
    cfg.add_section('NaxxIP')
    cfg.set('NaxxIP','IP','192.168.104.100')
    cfg.write(cfgfile)
    cfgfile.close()

### Returns a dict in order of AP's to be ran, ignoring the blanks in the ini
def GetConfig(cfg, sect):
    dct = {}

    opt = cfg.options(sect)
    for o in opt:
        try:
            dct[o] = cfg.get(sect,o)
        except:
            dct[o] = None

    return dct


"""cfg = ConfigParser.ConfigParser()
CreateConfig(cfg)
cfg.read('../config/default_config.ini')
dict1 = GetConfig(cfg, 'NaxxOutlet')
dict2 = GetConfig(cfg, 'NaxxIP')
dict3 = GetConfig(cfg, 'Throughput')
print dict1
print dict2
print dict3"""
