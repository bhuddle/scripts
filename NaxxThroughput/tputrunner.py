# Created by Benjamin Huddle, Allion USA
# Simple throughput runner for Amazon devices

import subprocess, sys, time, os, datetime, getopt, glob, multiprocessing
import ConfigParser, re
import lib.ConfigFile as CF
import lib.AdbController as adb
import lib.NAXXStrip as Nxx



def startIperf(dev):
    adb.adbShellCommand(dev, "iperf -s -w256K")


def main(argv):

    cfg = ConfigParser.ConfigParser()
    cfgfile = None
    processes = []

    try:
        opts, args = getopt.getopt(argv,"hc:g")
    except getopt.GetoptError:
        print 'sudo python tputrunner.py'
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print '\nUsage: $sudo python tputrunner.py\n'
            print 'OPTIONS'
            print '-h \t\tWill display the help menu'
            print '-c <config.ini>\tWill use the supplied config file in ./config/'
            print '\t\totherwise default_config.ini will be used for no arguments'
            print '-g \t\tWill generate a default config file'
            sys.exit()
        elif opt == '-c':
            # Use other config file
            cfgfile = arg
        elif opt == '-g':
            #create default config file
            CF.CreateConfig(cfg)
            print 'created default config file in ./config/'
            sys.exit()


    #Get list of devices
    adb.restartServer()
    sn = adb.checkIfDevice()
    if not sn:
        print "Please connect a device"
        sys.exit(0)


    #Use supplied config file or default
    if cfgfile != None:
        cfg.read('./config/'+cfgfile)
    else:
        cfg.read('./config/default_config.ini')


    #Get the IP of Naxx & ping
    NaxxIp = CF.GetConfig(cfg, "NaxxIP")
    ### Hardcoded for 8 ports ###
    Naxx = Nxx.NAXXStrip(NaxxIp['ip'], 8, './config/')
    if not Naxx.pinger():
        print "Unable to ping Naxx"
        sys.exit(0)


    #Turn off all ports on Naxx
    for i in range(1,9):
        socket = Nxx.Socket(NaxxIp['ip'],i)
        socket.Off()

    #Get dict of AP's to be ran
    Ports = CF.GetConfig(cfg, "NaxxOutlet")
    for port in sorted(Ports.keys()):
        adb.adbDisconnect()
        socket = Nxx.Socket(NaxxIp['ip'], port)
        socket.On()
        subnet = ""
        time.sleep(60) #SLEEP FOR AP TO BE WOKE UP
        #run throughput for each device attached.
        for dev in sn:
            adb.adbShellCommand(dev, "root")
            time.sleep(1)
            adb.adbShellCommand(dev, "remount")
            time.sleep(1)
            adb.adbShellCommand(dev, "input keyevent KEYCODE_WAKEUP")


            iperfProc = multiprocessing.Process(target=startIperf,args=(dev,))
            iperfProc.start()
            time.sleep(3)


            dev_ip = adb.adbShellCommand(dev, "netcfg | grep wlan0")
            output = adb.adbShellCommand(dev, "getprop | grep build")
            buildNo = re.search('(?<=\[ro.build.lab126.build\]: \[)\d+(?=\])',output).group()
            devName = re.search('(?<=\[ro.build.product\]: \[).*(?=\])',output).group()
            dev_ip = re.search('(\d+.\d+.\d+.\d+)', dev_ip).group()
            subnet = re.search('(\d+.\d+.\d+.)', dev_ip).group()
            if dev_ip == "0.0.0.0":
                print "DUT not connected to AP yet... sleeping another minute"
                time.sleep(60)
                fname = devName+"_"+buildNo+"_"+CF.GetConfig(cfg,"NaxxOutlet")[port]+".txt"
                os.system("echo 'Issue connecting to AP because IP= "+dev_ip+"' > "+fname)
            else:
                print "starting iperf on "+devName
                fname = devName+"_"+buildNo+"_"+CF.GetConfig(cfg,"NaxxOutlet")[port]+".txt"
                s = "iperf -c "+dev_ip+" -w256K -fm -l1470 -i1 -t"+CF.GetConfig(cfg,"Throughput")['time']+" > results/"+fname
                os.system(s)
            iperfProc.terminate()

        #Work around for adb connect devices
        nmap = "nmap -sP "+subnet+"*"
        print nmap
        output = subprocess.check_output(nmap.split(" "))
        ip_list = []
        for line in output.split('\n'):
            try:
                l = re.search('(\d+\.\d+\.\d+\.\d+)', line).group()
                ip_list.append(l)
            except: pass
        for ip in ip_list:
            conn = adb.adbConnect(str(ip))
            

        conndev = adb.checkIfDevice()

        for device in conndev:
            if ":" in device:
                try:
                    adb.adbShellCommand(device, "root")
                    time.sleep(1)
                    adb.adbConnect(device)
                    time.sleep(1)
                    adb.adbShellCommand(device, "remount")
                    time.sleep(1)
                    adb.adbShellCommand(device, "input keyevent KEYCODE_WAKEUP")
                    output = adb.adbShellCommand(device, "getprop | grep build")
                    devName = re.search('(?<=\[ro.build.product\]: \[).*(?=\])',output).group()
                    buildNo = re.search('(?<=\[ro.build.lab126.build\]: \[)\d+(?=\])',output).group()
                    dev_ip = re.search('(\d+.\d+.\d+.\d+)', dev_ip).group()
                    if devName == "margo":
                        iperfProc = multiprocessing.Process(target=startIperf,args=(device,))
                        iperfProc.start()
                        time.sleep(3)
                        print "running thoughput on "+devName
                        fname = devName+"_"+buildNo+"_"+CF.GetConfig(cfg,"NaxxOutlet")[port]+".txt"
                        s = "iperf -c "+dev_ip+" -w256K -fm -l1470 -i1 -t"+CF.GetConfig(cfg,"Throughput")['time']+" > results/"+fname
                        os.system(s)
                        iperfProc.terminate()
                        break
                    else:
                        continue
                except:
                    continue


        socket.Off()


    print "FINISHED -- see results in Results directory"



if __name__ == "__main__":
    main(sys.argv[1:])
