#include <iostream>
#include <string>
#include <algorithm>

/* C++ Program to check palindrome */

using namespace std;

int main () {
    string str1, str2;
    int len1, len2;

    cout << "Please enter a string: ";
    getline(cin, str1);
    cout << "Please enter another string: ";
    getline(cin, str2);
    len1 = str1.size();
    len2 = str2.size();

    if (len1 != len2) {
        cout << "The string lengths vary and they are not palindromes.\n";
        return 0;
    }
    else {
        sort(str1.begin(), str1.end());
        sort(str2.begin(), str2.end());
        cout << str1 << " " << str2 << endl;
        if (str1.compare(str2) == 0) {
            cout << "The strings are the same and palindromes\n";
        }
        else {
            cout << "The strings are not the same and are not palindromes\n";
        }
    }


    return 0;
}
