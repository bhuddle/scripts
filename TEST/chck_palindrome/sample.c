#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* C program to check palindrome */

int cmp(const void * a, const void * b) {
    return (*(const char *) a - *(const char *) b);
}


int main() {
    char str1[25], str2[25];
    long int len1, len2;
    printf("Please enter a word: ");
    fgets(str1, 25, stdin);
    len1 = strlen(str1);
    printf("Please enter another word: ");
    fgets(str2, 25, stdin);
    len2 = strlen(str2);


    if (len1 != len2) {
        printf("The lengths differ.  \nWord1 = %lu \nWord2 = %lu", len1, len2);
        return 0;
    }
    else {
        qsort(str1, len1, 1, cmp);
        qsort(str2, len2, 1, cmp);
        printf("word1: %s\nword2: %s\n", str1, str2);
        if (strcmp(str1, str2) == 0) {
            printf("The strings are palnidromes");
        }
        else {
            printf("The strings are not palindromes.");
        }
    }
    return 0;
}
