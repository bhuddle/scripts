# python file to detect palindrome

wrd1 = raw_input("Please enter a word: ")
wrd2 = raw_input("Please enter another word: ")

if (len(wrd1) != len(wrd2)):
    print "Lengths of the words are different and are not palindromes"
    exit(0)
else:
    if("".join(sorted(wrd1)) == "".join(sorted(wrd2))):
        print "The words are palindromes"
    else:
        print "The words are not palindromes"

#sort and compare
