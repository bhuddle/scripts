// program to remove any single character from string (all)
#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

int main() {
    string str1;
    char c;

    cout << "Please enter a string: ";
    getline(cin, str1);
    cout << "Please enter a character to remove: ";
    c = getchar();

    for(int i = 0; i < str1.size(); i++) {
        // check if the char is in the string array
        if(c == str1[i]) {
            str1[i] = '\0';
        }
    }
    cout << str1 << endl;

    return 0;
}
