#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* remove character from string */

int main() {
    char str1[25], str2[25];
    char c;
    int j = 0;
    printf("Please enter a word: ");
    fgets(str1, 25, stdin);
    printf("Please enter a character: ");
    do {
        c = getchar();
    } while (getchar() != '\n');

    printf("Word: %sChar: %c\n", str1, c);
    for(int i=0; i < strlen(str1); i++) {
        if(c != str1[i]) {
            // need to shift the chain down
            str2[j] = str1[i];
            j++;
            str2[j] = '\0';
        }
    }

    printf("\n%s", str2);

    return 0;
}
