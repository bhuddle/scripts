# created to get time off a logcat file.
# create 2 files, both grepped of the original logcat file
# Print the time in seconds to screen

import subprocess, sys, os, string
import time, getopt, re

def searchfiles(f, var):
    if var == "hdmi_msm_hdcp_enable":
        for line in f:
            time = re.search('hdmi_msm_hdcp_enable?', line)
            if time is not None:
                print line
                timestamp = re.search('\d\d:\d\d:\d\d.\d\d\d', line).group()
                return timestamp
    elif var == "AUTH_SUCCESS_INT":
        for line in f:
            time = re.search('AUTH_SUCCESS_INT?', line)
            if time is not None:
                print line
                timestamp = re.search('\d\d:\d\d:\d\d.\d\d\d', line).group()
                return timestamp
    elif var == "AUTH_FAIL_INT":
        for line in f:
            time = re.search('AUTH_FAIL_INT?', line)
            if time is not None:
                print line
                timestamp = re.search('\d\d:\d\d:\d\d.\d\d\d', line).group()
                return timestamp

def calculate(enable, fail, succ):
    #convert to time and subtract
    total = fail - enable
    print total

def main():
    if len(sys.argv) < 2:
        print "Please specify a logcat filename after script.  I.e. 'sudo timer.py logcat.txt'"
        sys.exit(2)
    else:
        filename = sys.argv[1]
        print filename
        f = open(filename, 'r')
        p = open(filename, 'r')
        c = open(filename, 'r')
        if not f:
            print "file does not exist"
            sys.exit(2)
        else:
            enable = searchfiles(f,"hdmi_msm_hdcp_enable")
            fail = searchfiles(p,"AUTH_FAIL_INT")
            success = searchfiles(c,"AUTH_SUCCESS_INT")
        f.close()
        p.close()
        c.close()

        calculate(enable, fail, success)

main()
