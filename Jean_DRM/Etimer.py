# created to get time off a logcat file.
# create 2 files, both grepped of the original logcat file
# Print the time in seconds to screen

import subprocess, sys, os, string
import time, getopt, re
from datetime import datetime

def adbSetup():
    adb = subprocess.Popen(["adb", "devices"], stdout=subprocess.PIPE)
    while adb.poll() is None:
        output = adb.stdout.readline()
        output = adb.stdout.readline()
        if "device" in output:
            if "device" in adb.stdout.readline():
                print "Multiple devices attached, exiting!"
                sys.exit(1)
            else:
                break
        else:
            print "No devices attached, exiting!"
            sys.exit(1)

    #one device at this point
    splitUp = output.split('\t')
    serialNo = splitUp[0]
    output = subprocess.Popen(["adb","root"], stdout=subprocess.PIPE)
    print output.stdout.readline()
    time.sleep(1)
    output = subprocess.Popen(["adb","remount"], stdout=subprocess.PIPE)
    print output.stdout.readline()
    time.sleep(1)
    output = subprocess.Popen(["adb","shell", "logcat", "-c"], sdtout=subprocess.PIPE)
    print "clearing logcat now..."
    print output.stdout.readline()
    time.sleep(1)
    return serialNo

def searchfiles(f, var):
    counter = 0
    if var == "hdmi_msm_hdcp_enable":
        for line in f:
            time = re.search('hdmi_msm_hdcp_enable?', line)
            if time is not None:
                print line
                timestamp = re.search('\d\d.\d\d\d\d\d\d', line).group()
                return timestamp
    elif var == "AUTH_SUCCESS_INT":
        for line in f:
            time = re.search('AUTH_SUCCESS_INT?', line)
            if time is not None:
                print line
                timestamp = re.search('\d\d.\d\d\d\d\d\d', line).group()
                return timestamp
    elif var == "AUTH_FAIL_INT":
        for line in f:
            time = re.search('AUTH_FAIL_INT?', line)
            if time is not None:
                print line
                counter += 1
                timestamp = re.search('\d\d.\d\d\d\d\d\d', line).group()
                #return timestamp
    return counter

def calculate(enable, succ):
    #convert to time and subtract
    #first = datetime.strptime(enable, '%S.%f')
    #success = datetime.strptime(succ, '%S.%f')
    first = float(enable)
    success = float(succ)
    SUCC = success - first
    print "Time to first success: " + str(SUCC) +"s"

def main():
    enable = 0
    success = 0
    #adbSetup()
    #print "Please start collecting logs with command below and press ENTER once HDMI is inserted back in "
    #raw_input("$ adb shell logcat -v time | tee logcat.txt")
    if len(sys.argv) < 2:
        print "Please specify a logcat filename after script.  I.e. 'sudo timer.py logcat.txt'"
        sys.exit(2)
    else:
        filename = sys.argv[1]
        #print filename
        f = open(filename, 'r')
        p = open(filename, 'r')
        c = open(filename, 'r')
        if not f:
            print "file does not exist"
            sys.exit(2)
        else:
            enable = searchfiles(f,"hdmi_msm_hdcp_enable")
            #print enable
            fail = searchfiles(p, "AUTH_FAIL_INT")
            #print fail
            success = searchfiles(c,"AUTH_SUCCESS_INT")
            #print success
        f.close()
        p.close()
        c.close()

        calculate(enable, success)
        print "Number of failure(s): " + str(fail)

main()
