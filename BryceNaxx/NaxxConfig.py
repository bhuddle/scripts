# Script that brings up Naxx & AP
# counts time that it was asleep for
# then logs it to a file

import time, math, os, sys, re, getopt, logging, subprocess
from NAXXStrip import NAXXStrip
from datetime import datetime

def pinger(ipaddr):
    find = None
    time = '1'
    ping = 'ping -c'+time+' '+ipaddr
    p = ping.split()
    try:
        out = subprocess.check_output(p)
        find = re.search(time+'\ received',out).group()
    except subprocess.CalledProcessError:
        find = None

    if find != None:
        return True
    else:
        return False


def checker(ip1, ip2, ip3):
    print "check the ip addresses"
    r1 = pinger(ip1)
    r2 = pinger(ip2)
    r3 = pinger(ip3)
    if r1 == True and r2 == True and r3 == True:
        return "pass"
    else: return "fail"


def sleeper(t):
    print "sleeping for {0} seconds".format(t)
    time.sleep(t)

def main(argv):
    logging.basicConfig(filename="runtime.log", filemode="w", level=logging.DEBUG)
    twon = 0
    thir = 0
    fort = 0
    fiver = 0
    dose = 0
    f1 = True
    f2 = True
    f3 = True

    try:
        opts, args = getopt.getopt(argv, "ha:d:n:")
    except getopt.GetoptError:
        print 'python NaxxConfig.py -h'
        sys.exit(2)


    for opt, arg in opts:
        if opt == '-h':
            print 'Useage: $sudo python NaxxConfig.py'
            print 'OPTIONS'
            print '-a <IP> where IP would be the AP IP address'
            print '-d <IP> where IP would be the DUT IP address'
            print '-n <IP> where IP would be the NAXX IP address'
            sys.exit()
        elif opt == '-a':
            AP = arg
        elif opt == '-d':
            DUT = arg
        elif opt == '-n':
            NAXX = arg
        elif len(argv) == 0:
            sys.exit(2)

    #make sure naxx is off before begining test
    strip = NAXXStrip(NAXX, 5, "")
    strip.TurnOn()
    time.sleep(5)

    #loop for 100 iterations
    for x in range(100):
        logging.info("Iteration {0}".format(str(x)))
        strip.TurnOff()
        logging.info("Naxx OFF @ "+time.strftime('%Y/%m/%d - %H:%M:%S'))
        #Sleep for a random time
        if f1:
            sleeper(1)
            f1 = False
        elif f2:
            sleeper(60)
            f2 = False
        elif f3:
            sleeper(300)
            f3 = False
        elif fiver < 5:
            sleeper(3600)
            fiver = fiver + 1
        elif dose < 2:
            sleeper(14400)
            dose = dose + 1
        elif twon < 20:
            sleeper(300)
            twon = town + 1
        elif fort < 40:
            sleeper(60)
            fort = fort + 1
        elif thir < 30:
            sleeper(600)
            thir = thir + 1

        #turn on Naxx
        strip.TurnOn()
        nax_time = None
        dut_time = None
        ap_time = None
        while not pinger(NAXX):
            if pinger(NAXX):
                nax_time = datetime.now()
                print nax_time
                break
        logging.info("Naxx ON @ "+nax_time.strftime('%Y/%m/%d - %H:%M:%S'))
        while not pinger(AP) and not pinger(DUT):
            if pinger(AP):
                ap_time = datetime.now()
                print ap_time
                logging.info("AP pingable @ "+ap_time.strftime('%Y/%m/%d - %H:%M:%S'))
            if pinger(DUT):
                dut_time = datetime.now()
                print dut_time
                logging.info("DUT pingable @ "+dut_time.strftime('%Y/%m/%d - %H:%M:%S'))
                wildcard = True
        print "both are up!"
        #do math
        result = dut_time - ap_time
        print result
        logging.info(">>>>>Time for DUT to connect: "+str(result))

if __name__ in "__main__":
    main(sys.argv[1:])
