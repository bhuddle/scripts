:: Fisher Script that will reboot computer every 1200 seconds
:: Add to startup folder under Start -> Programs -> Startup
:: Add shortcut from .vsb file to startup folder
@echo off

timeout /t 1200 /nobreak

shutdown.exe -r -t 00