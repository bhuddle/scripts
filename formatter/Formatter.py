# takes a file that will output for TWOFOUR_POST styled data

import subprocess, fileinput
import os, signal, sys
import time, string

original = open("/home/allion/scripts/formatter/original.txt", "r")
formatted_file = open("/home/allion/scripts/formatter/formatted_original.txt", "w")

try:
    for line in original:
        temp = line.split("=", 1)
        temp[1] = temp[1].rstrip()
        poo = "\""+temp[0]+"\""+":"+"\""+temp[1]+"\""+","+"\n"
        formatted_file.write(poo)
except: pass

try:
    for line in original:
        if line == "\n":
            continue
        temp = line.split(":", 1)
        temp[1] = temp[1].rstrip()
        temp[1] = temp[1].lstrip()
        poo = "\""+temp[0]+"\""+":"+"\""+temp[1]+"\""+","+"\n"
        formatted_file.write(poo)
except: pass

original.close()
formatted_file.close()
